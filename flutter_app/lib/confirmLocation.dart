import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:math';
import 'package:flutter_app/add_pickup.dart';

class ConfirmPage extends StatefulWidget {
  ConfirmPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ConfirmPageState createState() => _ConfirmPageState();
}

class _ConfirmPageState extends State<ConfirmPage> {
  GoogleMapController _controller; // user controller
  final CameraPosition _initialPosition = CameraPosition(target: LatLng(12.9716, 77.5946));

  final List<Marker> markers = []; // when we top on place it should show with marker

  addMarker(cordinate) {
    int id = Random().nextInt(100);

    setState(() {
      markers.add(Marker(position: cordinate, markerId: MarkerId(id.toString())));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Column(children: <Widget>[
        GoogleMap(
          initialCameraPosition: _initialPosition,
          mapType: MapType.normal,
          onMapCreated: (controller) {
            setState(() {
              _controller = controller;
            });
          },
          markers: markers.toSet(),
          onTap: (cordinate) {
            _controller.animateCamera(CameraUpdate.newLatLng(cordinate));
            //  addMarker(cordinate);
          },
        ),
        RaisedButton(
          textColor: Colors.white,
          color: Colors.red,
          padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
          child: Text('Confirm Location'),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddPickup()));
          },
        )
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _controller.animateCamera(CameraUpdate.zoomOut());
        },
        child: Icon(Icons.zoom_out),
      ),
    );
  }
}
