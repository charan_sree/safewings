import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_app/delivaryaddress.dart';

void main() => runApp(AddPickup());

class AddPickup extends StatelessWidget {
  // This widget i
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: AddpickupPage(
        storage: MyStorage(),
      ),
    );
  }
}

class MyStorage {
  // get directory path
  Future<String> get localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  //get file
  Future<File> get localFile async {
    final path = await localPath;
    return File('$path/man.txt');
  }

  //Read file contents
  Future<String> readContent() async {
    try {
      final file = await localFile;
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      return e; // Default null
    }
  }

  // Write file contents
  Future<File> writeContent(String content) async {
    final file = await localFile;
    return file.writeAsString('$content');
  }
}

class AddpickupPage extends StatefulWidget {
  final MyStorage storage;

  AddpickupPage({Key key, @required this.storage}) : super(key: key);
  @override
  _AddpickupPageState createState() => _AddpickupPageState();
}

class _AddpickupPageState extends State<AddpickupPage> {
  final controller = TextEditingController();
  final route = TextEditingController();
  final numbers = TextEditingController();
  String text = '';
  List<String> lst = [
    'Home',
    'Office',
    'Others'
  ];
  int selectedIndex = 0;

  void changeIndex(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  Widget customRadio(String txt, int index) {
    return OutlineButton(
      onPressed: () => changeIndex(index),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      borderSide: BorderSide(color: selectedIndex == index ? Colors.cyan : Colors.grey),
      child: Text(
        txt,
        style: TextStyle(color: selectedIndex == index ? Colors.cyan : Colors.grey),
      ),
    );
  }

  Future<File> saveToFile() async {
    setState(() {
      text = controller.text;
    });
    return widget.storage.writeContent(text);
  }

  @override
  void initState() {
    super.initState();
    widget.storage.readContent().then((String value) {
      setState(() {
        text = value;
      });
    });
  }

  @override
  void readFile() {
    widget.storage.readContent().then((String value) {
      setState(() {
        text = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
              hintText: 'Enter Location',
              labelText: 'Flat, Flore, Building Name',
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.black),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.teal),
              ),
              prefixIcon: Icon(Icons.home),
            ),
            controller: controller,
          ),
          SizedBox(height: 20),
          TextFormField(
            decoration: InputDecoration(
              hintText: 'Select Route',
              labelText: 'How to Reach (optional)',
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.black),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.teal),
              ),
              prefixIcon: Icon(Icons.near_me),
            ),
            controller: route,
          ),
          SizedBox(height: 20),
          TextFormField(
            decoration: InputDecoration(
              hintText: '8888888888',
              labelText: 'Contact Details (optional)',
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.black),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.teal),
              ),
              prefixIcon: Icon(Icons.phone_in_talk),
            ),
            controller: numbers,
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(0.0),
                child: RaisedButton(
                  onPressed: () {
                    saveToFile();
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => DeliveryApp()));
                  },
                  child: Text('Add PickUp Address'),
                  color: Colors.red,
                ),
              ),
              SizedBox(width: 10),
            ],
          ),
          Text('$text'),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              customRadio(lst[0], 0),
              customRadio(lst[1], 1),
              customRadio(lst[2], 2),
            ],
          ),
          SizedBox(height: 20),
          RaisedButton(
            onPressed: () {
              readFile();
            },
            child: Text("Read to Map"),
            color: Colors.red,
          ),
        ],
      ),
    );
  }
}
