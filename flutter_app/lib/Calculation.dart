import 'package:flutter/material.dart';

import 'package:flutter_app/payment.dart';

void main() => runApp(CalculatorApp());

class CalculatorApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: CalculatorPage(),
    );
  }
}

class CalculatorPage extends StatefulWidget {
  @override
  _CalculatorPageState createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  final t1 = TextEditingController();
  //  final t2 = TextEditingController();

  var num1 = 0, num2 = -1, mul = 0;

  void doMul() {
    setState(() {
      num1 = int.parse(t1.text);
      num2 = int.parse('23');
      mul = num1 * num2 + 2;
      /*   if (num2 == 1) {
        mul = num1 * num2;
      } else {
        mul = num1 * num2 - 1;
      } */
    });
  }

  void doClear() {
    setState(() {
      mul = 0;
      t1.text = "";
      //  t2.text = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(40.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Total Amount= ${mul} Rs",
              style: TextStyle(
                color: Colors.red,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            // Use the method to get distance insted of this textfield.
            TextField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: 'Enter number',
              ),
              controller: t1,
            ),
            /*   TextField(
              decoration: InputDecoration(
                hintText: 'Enter number',
              ),
              controller:t2,
            ), */
            SizedBox(height: 20),
            MaterialButton(
              color: Colors.blue,
              onPressed: doMul,
              child: Text(
                "*",
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.red,
                ),
              ),
            ),
            SizedBox(height: 20),
            MaterialButton(
              color: Colors.grey,
              onPressed: doClear,
              child: Text(
                'Clear',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            ),
            RaisedButton(
              textColor: Colors.white,
              color: Colors.red,
              padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
              child: Text('Checkout'),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => MyPayment()));
              },
            )
          ],
        ),
      ),
    );
  }
}
