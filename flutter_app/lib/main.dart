import 'package:flutter/material.dart';
import 'package:flutter_app/selectPackage.dart';

void main() {
  runApp(MaterialApp(
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _Safewings createState() => _Safewings();
}

class _Safewings extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
            padding: EdgeInsets.all(1),
            child: Column(
              children: <Widget>[
                Container(
                    color: Colors.red,
                    padding: EdgeInsets.fromLTRB(10, 50, 10, 50),
                    child: ListTile(
                      title: const Text('Save your Time!', style: TextStyle(color: Color(0xffffffff), fontWeight: FontWeight.bold, fontSize: 15)),
                      subtitle: const Text('Pickup or drop forgotten chargers,documents,food,laundry,items for repairs etc', style: TextStyle(color: Color(0xffffffff), fontSize: 11)),
                    )),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: TextField(
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: 'Pickup Address',
                      hintText: 'Search Pickup Location',
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: TextField(
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: 'Delivery Address',
                      hintText: 'Search Delivery Location',
                    ),
                  ),
                ),
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.red,
                  padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
                  child: Text('Proceed'),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => CheckboxWidget()));
                  },
                )
              ],
            )));
  }
}
