import 'package:flutter/material.dart';
import 'package:flutter_app/paymentList.dart';
import 'package:flutter_app/detailPage.dart';

void main() {
  runApp(MyPayment());
}

class MyPayment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyPaymentPage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyPaymentPage extends StatefulWidget {
  MyPaymentPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyPaymentPageState createState() => _MyPaymentPageState();
}

class _MyPaymentPageState extends State<MyPaymentPage> {
  List<Payment> payments;
  Payment selectedPayment;

  @override
  void initState() {
    super.initState();
    payments = Payment.getPayments();
  }

  setSelectedPay(Payment payment) {
    setState(() {
      selectedPayment = payment;
    });
  }

  List<Widget> paymentMethod() {
    List<Widget> widgets = [];
    for (Payment payment in payments) {
      widgets.add(
        RadioListTile(
          value: payment,
          groupValue: selectedPayment,
          title: Text(payment.title),
          subtitle: Text(payment.subTitle),
          onChanged: (payment) {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailsPage(payment)));
            //setSelectedPay(currentPayment);
          },
          selected: selectedPayment == payment,
          activeColor: Colors.grey,
        ),
        // GestureDetector(
        //   onTap: (payment) {
        //     Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailsPage(payment)));
        //   },
        // ),
      );
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0x00000000),
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Select payment method',
              style: TextStyle(color: Color(0xff000000), fontSize: 20),
            ),
            Visibility(
              visible: true,
              child: Text(
                'Item Total 55',
                style: TextStyle(
                  fontSize: 18,
                  color: Color(0xFF8a8a92),
                ),
              ),
            ),
          ],
        ),
        elevation: 0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            //padding: EdgeInsets.all(20.0),
            child: Text(
              "Wallets",
              style: TextStyle(
                fontSize: 20,
                color: Color(0xFF8a8a92),
              ),
            ),
          ),
          Column(
            children: paymentMethod(),
          ),
        ],
      ),
    );
  }
}
