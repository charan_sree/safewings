import 'package:flutter/material.dart';
import 'package:flutter_app/paymentList.dart';

class DetailsPage extends StatefulWidget {
  final Payment payment;

  const DetailsPage(this.payment, {Key key}) : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.payment.title),
      ),
      body: Center(
        child: Column(
          children: [
            const SizedBox(height: 16),
            Icon(
              widget.payment.icon,
              size: 100,
            ),
            const SizedBox(height: 16),
            Text(
              'Item description: ${widget.payment.description}',
              style: TextStyle(fontSize: 18),
            )
          ],
        ),
      ),
    );
  }
}
